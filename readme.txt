
TWS API RAW

PURPOSE:
--------

  1. to study recently published new IB API (Version: API 9.69, Release Date: July 1 2012)

  2. to run few experiments (to play with parameters, to some simple benchmarks etc.)


HOW TO RUN:
--------------------

  1. change directory to:  API.v.9.69/test-app/TestPosix

  2. run 'make' to compile

  3. in successful case you'd see compiled executable file: PosixSocketClientTest (in "test-app/TestPosix" folder)
     run it !

  4. enjoy! :)

(tested on OS-X and Ubuntu 12.04, works perfectly fine!-)


MORE DETAILED INFO:
------------------

  For more details please read version-specific readmies:

    twsapiraw/API.v.9.69/readme.API.v.9.69.txt
