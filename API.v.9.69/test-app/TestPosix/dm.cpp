#include "PosixTestClient.h"

#include "EPosixClientSocket.h"
#include "EPosixClientSocketPlatform.h"

#include "Contract.h"
#include "Order.h"

#ifdef _WIN32
# include <Windows.h>
# define sleep( seconds) Sleep( seconds * 1000);
#else
# include <unistd.h>
#endif

#include <cstring>  // for strcpy()
#include <stdarg.h>


// #include "PosixTestClient.h"

//------------------------------------------------------------- only for osx compatability (begin)
// http://stackoverflow.com/questions/5167269/clock-gettime-alternative-in-mac-os-x
#ifdef __MACH__
#define CLOCK_REALTIME 0
#define CLOCK_MONOTONIC 0
#include <sys/time.h>
//clock_gettime is not implemented on OSX
int clock_gettime(int /*clk_id*/, struct timespec* t)
{
    struct timeval now;
    int rv = gettimeofday(&now, NULL);
    if (rv) return rv;
    t->tv_sec  = now.tv_sec;
    t->tv_nsec = now.tv_usec * 1000;
    return 0;
}

#endif
//------------------------------------------------------------- only for osx compatability (end)

#include <memory> // unique_ptr’ is not a member of ‘std’
#include "dm.h"

// see header for deatails
void DM::timespec_substract(struct timespec *result,
             const struct timespec *lhs,
             const struct timespec *rhs)
{
     result->tv_sec = lhs->tv_sec - rhs->tv_sec;
     if (lhs->tv_nsec >= rhs->tv_nsec)
         result->tv_nsec = lhs->tv_nsec - rhs->tv_nsec;
     else {
         result->tv_sec -= 1;
         result->tv_nsec = lhs->tv_nsec + (1000000000 - rhs->tv_nsec);
     }
}


std::string DM::string_format(const std::string fmt_str, ...) {
    int final_n, n = fmt_str.size() * 2; /* reserve 2 times as much as the length of the fmt_str */
    std::string str;
    std::unique_ptr<char[]> formatted;
    va_list ap;
    while(1) {
        formatted.reset(new char[n]); /* wrap the plain char array into the unique_ptr */
        std::strcpy(&formatted[0], fmt_str.c_str());
        va_start(ap, fmt_str);
        final_n = vsnprintf(&formatted[0], n, fmt_str.c_str(), ap);
        va_end(ap);
        if (final_n < 0 || final_n >= n)
            n += abs(final_n - n + 1);
        else
            break;
    }
    return std::string(formatted.get());
}



