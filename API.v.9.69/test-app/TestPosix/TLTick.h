#ifndef tltick_h__INCLUDED
#define tltick_h__INCLUDED

#include <stdio.h>
#include <string>

#include "dm.h"

class TLTick
{
	public:
		TLTick(void);
		~TLTick(void);
		std::string sym;
		int symid;
		int date;
		int time;
		//int depth;

		double trade;
		int trade_size;

		double bid;
		double ask;

		int bid_size;
		int ask_size;

		//std::string bid_ex;
		//std::string asx_ex;
		//std::string ex;
		bool isValid();
		bool isTrade();
		bool hasBid();
		bool hasAsk();
		std::string Serialize(void) const;
		static TLTick Deserialize(std::string message);
};
/*
enum TickField
{ // tick message fields from TL server
        ksymbol = 0,
        kdate,
        ktime,
        KUNUSED,
        ktrade,
        ktsize,
        ktex,
        kbid,
        kask,
        kbidsize,
        kasksize,
        kbidex,
        kaskex,
	ktdepth,
};
*/
#endif
