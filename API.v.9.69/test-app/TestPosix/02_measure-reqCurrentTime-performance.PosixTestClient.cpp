/* Copyright (C) 2013 Interactive Brokers LLC. All rights reserved. This code is subject to the terms
 * and conditions of the IB API Non-Commercial License or the IB API Commercial License, as applicable. */

#include "PosixTestClient.h"

#include "EPosixClientSocket.h"
#include "EPosixClientSocketPlatform.h"

#include "Contract.h"
#include "Order.h"

#include "tick_type_str.h"

const int PING_DEADLINE = 2; // seconds
const int SLEEP_BETWEEN_PINGS = 5; // seconds

//------------------------------------------------------------- only for osx compatability (begin)
// http://stackoverflow.com/questions/5167269/clock-gettime-alternative-in-mac-os-x
#ifdef __MACH__
#define CLOCK_REALTIME 0
#define CLOCK_MONOTONIC 0
#include <sys/time.h>
//clock_gettime is not implemented on OSX
int clock_gettime(int /*clk_id*/, struct timespec* t)
{
    struct timeval now;
    int rv = gettimeofday(&now, NULL);
    if (rv) return rv;
    t->tv_sec  = now.tv_sec;
    t->tv_nsec = now.tv_usec * 1000;
    return 0;
}

#endif
//------------------------------------------------------------- only for osx compatability (end)

///////////////////////////////////////////////////////////
// member funcs
PosixTestClient::PosixTestClient()
	: m_pClient(new EPosixClientSocket(this))
	, m_state(ST_CONNECT)
	, m_sleepDeadline(0)
	, m_orderId(0)
{
	dout << "PosixTestClient constructor\n";
}

PosixTestClient::~PosixTestClient()
{
	dout << "PosixTestClient destructor\n";
}

bool PosixTestClient::connect(const char *host, unsigned int port, int clientId)
{
	dout << "PosixTestClient::connect()\n";

	// trying to connect
	printf( "\t Connecting to %s:%d clientId:%d\n", !( host && *host) ? "127.0.0.1" : host, port, clientId);

	bool bRes = m_pClient->eConnect( host, port, clientId);

	if (bRes) {
		printf( "\t Connected to %s:%d clientId:%d\n", !( host && *host) ? "127.0.0.1" : host, port, clientId);
	}
	else
		printf( "\t Cannot connect to %s:%d clientId:%d\n", !( host && *host) ? "127.0.0.1" : host, port, clientId);

	return bRes;
}

void PosixTestClient::disconnect() const
{
	dout << "PosixTestClient::disconnect()\n";
	m_pClient->eDisconnect();

	printf ( "\t Disconnected\n");
}

bool PosixTestClient::isConnected() const
{
	//dout << "PosixTestClient::isConnected()\n";
	return m_pClient->isConnected();
}

void PosixTestClient::processMessages()
{
	fd_set readSet, writeSet, errorSet;

	struct timeval tval;
	tval.tv_usec = 0;
	tval.tv_sec = 0;

	time_t now = time(NULL);

	switch (m_state) {
		case ST_PLACEORDER:
			//placeOrder();
			reqMktData_CAD_JPY();
			break;
		case ST_PLACEORDER_ACK:
			break;
		case ST_CANCELORDER:
			cancelOrder();
			break;
		case ST_CANCELORDER_ACK:
			break;
		case ST_PING:
			reqCurrentTime();
			break;
		case ST_PING_ACK:
			if( m_sleepDeadline < now) {
				disconnect();
				return;
			}
			break;
		case ST_IDLE:
			if( m_sleepDeadline < now) {
				m_state = ST_PING;
				return;
			}
			break;
	}

	if( m_sleepDeadline > 0) {
		// initialize timeout with m_sleepDeadline - now
		tval.tv_sec = m_sleepDeadline - now;
	}

	if( m_pClient->fd() >= 0 ) {

		FD_ZERO( &readSet);
		errorSet = writeSet = readSet;

		FD_SET( m_pClient->fd(), &readSet);

		if( !m_pClient->isOutBufferEmpty())
			FD_SET( m_pClient->fd(), &writeSet);

		FD_CLR( m_pClient->fd(), &errorSet);

		int ret = select( m_pClient->fd() + 1, &readSet, &writeSet, &errorSet, &tval);

		if( ret == 0) { // timeout
			return;
		}

		if( ret < 0) {	// error
			disconnect();
			return;
		}

		if( m_pClient->fd() < 0)
			return;

		if( FD_ISSET( m_pClient->fd(), &errorSet)) {
			// error on socket
			m_pClient->onError();
		}

		if( m_pClient->fd() < 0)
			return;

		if( FD_ISSET( m_pClient->fd(), &writeSet)) {
			// socket is ready for writing
			m_pClient->onSend();
		}

		if( m_pClient->fd() < 0)
			return;

		if( FD_ISSET( m_pClient->fd(), &readSet)) {
			// socket is ready for reading
			m_pClient->onReceive();
		}
	}
}

//////////////////////////////////////////////////////////////////
// methods
void PosixTestClient::reqCurrentTime()
{
	dout << "reqCurrentTime() PING_DEADLINE=" << PING_DEADLINE << "sec \n";

	// set ping deadline to "now + n seconds"
	m_sleepDeadline = time( NULL) + PING_DEADLINE;

	m_state = ST_PING_ACK;

	// http://stackoverflow.com/questions/538609/high-resolution-timer-with-c-and-linux
	// clock_gettime(CLOCK_MONOTONIC, &ts); // Works on FreeBSD
	clock_gettime(CLOCK_REALTIME, &high_resolution_ts); // Works on Linux

	m_pClient->reqCurrentTime();
}

void PosixTestClient::placeOrder()
{
	dout << "placeOrder()\n";
	Contract contract;
	Order order;

	contract.symbol = "ABX"; // "MSFT";
	contract.secType = "STK";
	contract.exchange = "SMART";
	contract.currency = "USD";

	order.action = "BUY";
	order.totalQuantity = 1000;
	order.orderType = "LMT";
	order.lmtPrice = 0.01;

	printf( "\t Placing Order %ld: %s %ld %s at %f\n", m_orderId, order.action.c_str(), order.totalQuantity, contract.symbol.c_str(), order.lmtPrice);

	m_state = ST_PLACEORDER_ACK;

	m_pClient->placeOrder( m_orderId, contract, order);
}

void PosixTestClient::cancelOrder()
{
	dout << "cancelOrder()\n";
	printf( "\t Cancelling Order %ld\n", m_orderId);

	m_state = ST_CANCELORDER_ACK;

	m_pClient->cancelOrder( m_orderId);

}

void PosixTestClient::reqMktData_CAD_JPY()
{
	dout << "\t . . . trying to subscribe by reqMktData()\n";

	Contract contract;

	contract.symbol = "";			// "CAD.JPY"<- this is no go, leave symbol blank for forex pair
	contract.conId=0;
	contract.localSymbol = "CAD.JPY";
	contract.secType = "CASH";
	contract.exchange = "IDEALPRO";
	contract.currency = "JPY";		//  "CAD"<- cad is no go, set 2nd currency name here

	TickerId id = 0;
	PosixTestClient::reqMktData( id, contract, "", false);
	// on success you'd receive tons of tickPrice() and tickSize() events
	// on failure usually you get somthing like:
	//	PosixTestClient.cpp:293 error(): id=-1 errorCode=2104, msg=Market data farm connection is OK:cashfarm
	//	PosixTestClient.cpp:295 error(): id=0 errorCode=200, msg=No security definition has been found for the request

	m_state = ST_PING;
/*
 other possible required values to subscribe to CAD.JPY are:
// conId = 0
// symbol = ""
// localSybmol = "CAD.JPY"
// expiry = 0
// strike = 0.0
// right = ""
// multiplier = "1"
// primaryExchange = ""
// currency = "JPY"
// tradingClass = ""
// includeExpired = false
// secIdType = ""
// secId = ""
// comboLetsDescrip = ""
// comboLets ptr_ = 0x0
// underComp = 0x0

*/

}

///////////////////////////////////////////////////////////////////
// events
void PosixTestClient::orderStatus( OrderId orderId, const IBString &status, int filled,
	   int remaining, double avgFillPrice, int permId, int parentId,
	   double lastFillPrice, int clientId, const IBString& whyHeld)

{
	dout << "orderStatus()\n"
	     << "\t orderId: " << orderId << "\n"
	     << "\t status: " << status << "\n"
	     << "\t filled: " << filled << "\n"
	     << "\t remaining: " << remaining << "\n"
	     << "\t avgFillPrice" << avgFillPrice << "\n"
	     << "\t permId" << permId << "\n"
	     << "\t parentId" << parentId << "\n"
	     << "\t lastFillPrice" << lastFillPrice << "\n"
	     << "\t clientId" << clientId << "\n"
	     << "\t whyHeld" << whyHeld << "\n";

	if( orderId == m_orderId) {
		if( m_state == ST_PLACEORDER_ACK && (status == "PreSubmitted" || status == "Submitted"))
			m_state = ST_CANCELORDER;

		if( m_state == ST_CANCELORDER_ACK && status == "Cancelled")
			m_state = ST_PING;

		printf( "\t Order: id=%ld, status=%s\n", orderId, status.c_str());
	}
}

void PosixTestClient::nextValidId( OrderId orderId)
{
	dout << "nextValidId(): orderId" << orderId << "\n";
	m_orderId = orderId;

	// 1st "goto" is here:
	//m_state = ST_PLACEORDER;
	m_state = ST_PING;
}

void PosixTestClient::timespec_substract(struct timespec *result,
             const struct timespec *lhs,
             const struct timespec *rhs)
{
     result->tv_sec = lhs->tv_sec - rhs->tv_sec;
     if (lhs->tv_nsec >= rhs->tv_nsec)
         result->tv_nsec = lhs->tv_nsec - rhs->tv_nsec;
     else {
         result->tv_sec -= 1;
         result->tv_nsec = lhs->tv_nsec + (1000000000 - rhs->tv_nsec);
     }
}


void PosixTestClient::currentTime( long time)
{
	dout << "currentTime() time=" << time << "\n";
	if ( m_state == ST_PING_ACK) {
		time_t t = ( time_t)time;
		struct tm * timeinfo = localtime ( &t);

		// http://stackoverflow.com/questions/538609/high-resolution-timer-with-c-and-linux
		// clock_gettime(CLOCK_MONOTONIC, &ts); // Works on FreeBSD
		timespec high_resolution_ts2, delta_ts;
		clock_gettime(CLOCK_REALTIME, &high_resolution_ts2); // Works on Linux

		timespec_substract( &delta_ts, &high_resolution_ts2, &high_resolution_ts);

		printf( "\t Current date/time is: %s", asctime( timeinfo));
		printf( "\t Request-response roundrip time: %lld.%.9ld sec\n", (long long)delta_ts.tv_sec, delta_ts.tv_nsec);	// http://stackoverflow.com/questions/8304259/formatting-struct-timespec

		time_t now = ::time(NULL);
		m_sleepDeadline = now + SLEEP_BETWEEN_PINGS;

		m_state = ST_IDLE;
	}
}
void PosixTestClient::error(const int id, const int errorCode, const IBString errorString)
{
//	printf( "Error id=%d, errorCode=%d, msg=%s\n", id, errorCode, errorString.c_str());
	dout << "error(): id=" << id << " errorCode=" << errorCode << ", msg=" << errorString << "\n";

	if( id == -1 && errorCode == 1100) // if "Connectivity between IB and TWS has been lost"
		disconnect();
}

void PosixTestClient::tickPrice( TickerId tickerId, TickType field, double price, int canAutoExecute) {
	dout << " tickPrice(): tickerId=" << tickerId << " TickType=" << TickTypeStr[field] << " price=" << price << " canAutoExecute=" << canAutoExecute << "\n";
}

void PosixTestClient::tickSize( TickerId tickerId, TickType field, int size) {
	dout << " tickSize(): tickerId=" << tickerId << " TickType=" << TickTypeStr[field] << " size=" << size << "\n";
}
void PosixTestClient::tickOptionComputation( TickerId tickerId, TickType tickType, double impliedVol, double delta,
					 double optPrice, double pvDividend,
					 double gamma, double vega, double theta, double undPrice) {
	dout << " tickOptionComputation(): " << "\n";
}
void PosixTestClient::tickGeneric(TickerId tickerId, TickType tickType, double value) {
	dout << " tickGeneric(): " << "\n";
}

void PosixTestClient::tickString(TickerId tickerId, TickType tickType, const IBString& value) {
	dout << " tickString(): " << "\n";
}

void PosixTestClient::tickEFP(TickerId tickerId, TickType tickType, double basisPoints, const IBString& formattedBasisPoints,
			   double totalDividends, int holdDays, const IBString& futureExpiry, double dividendImpact, double dividendsToExpiry) {
	dout << " tickEFP(): " << "\n";
}

void PosixTestClient::openOrder( OrderId orderId, const Contract&, const Order&, const OrderState& ostate) {
	dout << " openOrder(): " << "\n";
}

void PosixTestClient::openOrderEnd() {
	dout << " openOrderEnd(): " << "\n";
}

void PosixTestClient::winError( const IBString &str, int lastError) {
	dout << " winError(): " << "\n";
}

void PosixTestClient::connectionClosed() {
	dout << " connectionClosed(): " << "\n";
}

void PosixTestClient::updateAccountValue(const IBString& key, const IBString& val,
					  const IBString& currency, const IBString& accountName) {
	dout << " updateAccountValue(): " << "\n";
}

void PosixTestClient::updatePortfolio(const Contract& contract, int position,
		double marketPrice, double marketValue, double averageCost,
		double unrealizedPNL, double realizedPNL, const IBString& accountName){
	dout << " updatePortfolio(): " << "\n";
}

void PosixTestClient::updateAccountTime(const IBString& timeStamp) {
	dout << " updateAccountTime(): " << "\n";
}

void PosixTestClient::accountDownloadEnd(const IBString& accountName) {
	dout << " accountDownloadEnd(): " << "\n";
}

void PosixTestClient::contractDetails( int reqId, const ContractDetails& contractDetails) {
	dout << " contractDetails(): " << "\n";
}

void PosixTestClient::bondContractDetails( int reqId, const ContractDetails& contractDetails) {
	dout << " bondContractDetails(): " << "\n";
}

void PosixTestClient::contractDetailsEnd( int reqId) {
	dout << " contractDetailsEnd(): " << "\n";
}

void PosixTestClient::execDetails( int reqId, const Contract& contract, const Execution& execution) {
	dout << " execDetails(): " << "\n";
}

void PosixTestClient::execDetailsEnd( int reqId) {
	dout << " execDetailsEnd(): " << "\n";
}

void PosixTestClient::updateMktDepth(TickerId id, int position, int operation, int side,
									  double price, int size) {
	dout << " updateMktDepth(): " << "\n";
}

void PosixTestClient::updateMktDepthL2(TickerId id, int position, IBString marketMaker, int operation,
										int side, double price, int size) {
	dout << " updateMktDepthL2(): " << "\n";
}

void PosixTestClient::updateNewsBulletin(int msgId, int msgType, const IBString& newsMessage, const IBString& originExch) {
	dout << " updateNewsBulletin(): " << "\n";
}

void PosixTestClient::managedAccounts( const IBString& accountsList) {
	dout << " managedAccounts(): " << "\n";
}

void PosixTestClient::receiveFA(faDataType pFaDataType, const IBString& cxml) {
	dout << " receiveFA(): " << "\n";
}

void PosixTestClient::historicalData(TickerId reqId, const IBString& date, double open, double high,
									  double low, double close, int volume, int barCount, double WAP, int hasGaps) {
	dout << " historicalData(): " << "\n";
}

void PosixTestClient::scannerParameters(const IBString &xml) {
	dout << " scannerParameters(): " << "\n";
}

void PosixTestClient::scannerData(int reqId, int rank, const ContractDetails &contractDetails,
	   const IBString &distance, const IBString &benchmark, const IBString &projection,
	   const IBString &legsStr) {
	dout << " scannerData(): " << "\n";
}

void PosixTestClient::scannerDataEnd(int reqId) {
	dout << " scannerDataEnd(): " << "\n";
}

void PosixTestClient::realtimeBar(TickerId reqId, long time, double open, double high, double low, double close,
								   long volume, double wap, int count) {
	dout << " realtimeBar(): " << "\n";
}

void PosixTestClient::fundamentalData(TickerId reqId, const IBString& data) {
	dout << " fundamentalData(): " << "\n";
}

void PosixTestClient::deltaNeutralValidation(int reqId, const UnderComp& underComp) {
	dout << " deltaNeutralValidation(): " << "\n";
}

void PosixTestClient::tickSnapshotEnd(int reqId) {
	dout << " tickSnapshotEnd(): " << "\n";
}

void PosixTestClient::marketDataType(TickerId reqId, int marketDataType) {
	dout << " marketDataType(): " << "\n";
}

void PosixTestClient::commissionReport( const CommissionReport& commissionReport) {
	dout << " commissionReport(): " << "\n";
}

void PosixTestClient::position( const IBString& account, const Contract& contract, int position, double avgCost) {
	dout << " position(): " << "\n";
}

void PosixTestClient::positionEnd() {
	dout << " positionEnd(): " << "\n";
}

void PosixTestClient::accountSummary( int reqId, const IBString& account, const IBString& tag, const IBString& value, const IBString& curency) {
	dout << " accountSummary(): " << "\n";
}

void PosixTestClient::accountSummaryEnd( int reqId) {
	dout << " accountSummaryEnd(): " << "\n";
}

// from TWS_TLServer.cpp:
// 			this->m_link[this->validlinkids[0]]->reqMktData((TickerId)stockticks.size(),contract,"",false);
//
//
void PosixTestClient::reqMktData( TickerId id, const Contract &contract,
	   const IBString& genericTicks, bool snapshot) {
	dout << " reqMktData(): " << "\n";
	m_pClient->reqMktData( id, contract, genericTicks, snapshot);

/*
	TickerId id = 0;

	Contract contract;

	contract.symbol = "CAD.JPY";
	contract.secType = "CASH";
	contract.exchange = "IDEALPRO";
	contract.currency = "CAD";
*/
	// PosixTestClient::reqMktData( id, contract, "", false);

/*
 other possible required values to subscribe to CAD.JPY are:
// conId = 0
// symbol = ""
// localSybmol = "CAD.JPY"
// expiry = 0
// strike = 0.0
// right = ""
// multiplier = "1"
// primaryExchange = ""
// currency = "JPY"
// tradingClass = ""
// includeExpired = false
// secIdType = ""
// secId = ""
// comboLetsDescrip = ""
// comboLets ptr_ = 0x0
// underComp = 0x0

*/
}


