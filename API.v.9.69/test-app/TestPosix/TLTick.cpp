#include "TLTick.h"

	TLTick::TLTick(void)
	{
		sym = "";
		symid = -1;

		date = 0;
		time = 0;

		trade = 0;
		trade_size = 0;
		
		bid_size = 0;
		ask_size = 0;
		bid = 0;
		ask = 0;
		//ex = "";
		//bid_ex = "";
		//ask_ex = "";
		//depth = 0;
	}
	bool TLTick::isTrade()
	{
	    return (sym != "") && (trade_size*trade != 0);
	}
	bool TLTick::hasAsk() { return (sym != "") && (ask*ask_size != 0); }
	bool TLTick::hasBid() { return (sym != "") && (bid*bid_size != 0); }
	bool TLTick::isValid()
	{
		return (sym!="") && (isTrade() || hasAsk() || hasBid());
	}

	std::string TLTick::Serialize(void) const
	{
		std::string result;
		char delim = ',';
		result += sym;
		result += delim;
		result += DM::string_format("%i,%i,,", date, time);
		result += DM::string_format("%f", trade);
		result += delim;
		result += DM::string_format("%i", trade_size);
		result += delim;
		//result += ex;
		//result += delim;
		result += DM::string_format("%f,%f,", bid, ask);
		result += DM::string_format("%i,%i", bid_size, ask_size);
		//result += delim;
		//result += bid_ex;
		//result += delim;
		//result += ask_ex;
		//result += delim;
		//result += DM::string_format("%i", depth);
		
		//result.Format(_T("%s,%i,%i,,%f,%i,%s,%f,%f,%i,%i,%s,%s,%i"),sym,date,time,trade,size,ex,bid,ask,bs,os,be,oe,depth);
		
		return result;
	}
	TLTick TLTick::Deserialize(std::string message)
	{
		TLTick k;
		/*

		not yet implemented (we don't need it for now)
		and serialize() is used for debug purpoeses only

		std::vector<std::string> r;
		gsplit(message,_T(","),r);
		k.sym = r[ksymbol];
		k.date = _tstoi(r[kdate]);
		k.time = _tstoi(r[ktime]);
		k.trade = _tstof(r[ktrade]);
		k.size = _tstoi(r[ktsize]);
		k.ex = r[ktex];
		k.bid = _tstof(r[kbid]);
		k.ask = _tstof(r[kask]);
		k.bs = _tstoi(r[kbidsize]);
		k.os = _tstoi(r[kasksize]);
		k.be = r[kbidex];
		k.oe = r[kaskex];
		k.depth = _tstoi(r[ktdepth]);
		*/
		return k;
	}

	TLTick::~TLTick(void)
	{

	}

